const
  gulp = require('gulp'),
  exec = require('child_process').exec,
  { sass } = require('@mr-hope/gulp-sass');

gulp.task('build:css', () => {
  return gulp.src("scss/**/*.scss")
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./dist/css'));
});

gulp.task('build:nemron', (cb) => {
  exec('npm run rollup', (err) => {
    if (err) return cb(err);
    cb();
  });
});

gulp.task('build:all', gulp.series('build:css', 'build:nemron'));

gulp.task('watch', () => {
  gulp.watch(['./nemron-elements.js'], gulp.series('build:nemron'));
  gulp.watch('scss/**/*.scss', gulp.series('build:css'));
});
