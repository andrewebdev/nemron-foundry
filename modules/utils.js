export class DicePool {
  static get dScale() {
    return [
      "d4",  // 0
      "d6",
      "d8",
      "d10",
      "d12",
    ];
  };

  constructor() {
    this.resetPool();
  }

  resetPool() {
    this.resetNodes();
    this.resetBoonPenalty();
  }

  resetNodes() {
    this._nodes = [];
  }

  resetBoonPenalty() {
    this._bpMap = {
      "boon": null,  // Used to record lesser and greater boon
      "penalty": null,  // Used to record lesser and greater penalty
    };
  }

  get initiative() {
    var ini = 0;
    this._nodes.forEach(n => {
      if (n.useInitiative) ini++;
    });
    return ini;
  }

  get energyCost() {
    var energy = 0;
    this._nodes.forEach(n => {
      if (n.energyCost) energy += n.energyCost;
    });
    return energy;
  }

  getRollFormula() {
    var rollParts = [];
    var keepVal = 'kh';

    if (this._bpMap.penalty !== null && this._bpMap.boon !== null) {
      throw "Cannot have both boon & penalty in a dice pool";
    }

    if (this._bpMap.penalty !== null) { keepVal = 'kl'; }

    // Cycle through every node in the pool and prep the roll parts
    this._nodes.forEach((n) => {
      let dType = DicePool.dScale[n.rank];
      rollParts.push(`1${dType}`)
    });

    // Now add boons or penalties
    if (this._bpMap.boon !== null) {
      let dType = DicePool.dScale[this._bpMap.boon];
      rollParts.push(`1${dType}`);
    } else if (this._bpMap.penalty !== null) {
      let dType = DicePool.dScale[this._bpMap.penalty];
      rollParts.push(`1${dType}`);
    }

    var rollFormula = rollParts.join(',');
    return `{${rollFormula}}${keepVal}`;
  }

  addNode({id, rank=0, useInitiative=true, energyCost=0}) {
    this._nodes.push({
      id: id,
      rank: rank,
      useInitiative: useInitiative,
      energyCost: energyCost,
    });
  }

  removeNode(nodeId) {
    var _nodeIndex = this._nodes.findIndex(n => n.id === nodeId);
    this._nodes.splice(_nodeIndex, 1);
  }

  addLesserBoon() {
    // Lesser can only be added if we don't have a greater penalty
    if (this._bpMap.boon === null && this._bpMap.penalty !== 0) {
      this._bpMap.boon = 2;

      if (this._bpMap.penalty === 2) {
        // 2 lessers cancel each other out
        this.resetBoonPenalty();
      }
    }
  }

  addGreaterBoon() {
    this._bpMap.boon = 4;

    if (this._bpMap.penalty === 0) {
      // 2 greaters cancel each other out
      this.resetBoonPenalty();
    }
  }

  addLesserPenalty() {
    if (this._bpMap.penalty === null && this._bpMap.boon !== 4) {
      this._bpMap.penalty = 2;

      if (this._bpMap.boon === 2) {
        // 2 lessers cancel each other out
        this.resetBoonPenalty();
      }
    }
  }

  addGreaterPenalty() {
    this._bpMap.penalty = 0;

    if (this._bpMap.boon === 4) {
      // 2 greaters cancel each other out
      this.resetBoonPenalty();
    }
  }
}


export class NemRoll {

  static rollPool(pool, mod) {
    // Reset the pool boon/penalties before making the roll
    // TODO/FIXME: This is a temporary measure as we don't relaly know yet what
    // a nice UX/UI would be most beneficial here for players. For the time
    // being we will assume all players and GM's keep track of when they need to
    // use a boon/penalty
    pool.resetBoonPenalty();

    switch (mod) {
      case 'gPenalty':
        pool.addGreaterPenalty();
        break;

      case 'lPenalty':
        pool.addLesserPenalty();
        break;

      case 'lBoon':
        pool.addLesserBoon();
        break;

      case 'gBoon':
        pool.addGreaterBoon();
        break;
    }

    return {
      pool: pool,
      roll: NemRoll.simple({formula: pool.getRollFormula()}),
    };
  }

  static async action({token=null, actor=null}) {
    const template = "systems/nemron/templates/dialogs/action-dialog.hbs";

    // Prepare the context for the template
    var context = {};
    if (token) {
      context.tokenId = token.id;
    } else {
      context.actorId = actor.id;
    }

    // Render the template
    const html = await renderTemplate(template, context);

    return new Promise(resolve => {
      new Dialog({
        title: game.i18n.localize("nem.dialogs.actionDialog.title"),
        content: html,
        buttons: {
          'gPenalty': {
            label: game.i18n.localize("nem.rollTypes.gPenalty"),
            callback: html => {
              resolve(NemRoll.rollPool(html[0].querySelector('nem-pool').pool, 'gPenalty'));
            },
          },

          'lPenalty': {
            label: game.i18n.localize("nem.rollTypes.lPenalty"),
            callback: html => {
              resolve(NemRoll.rollPool(html[0].querySelector('nem-pool').pool, 'lPenalty'));
            },
          },

          'normal': {
            label: game.i18n.localize("nem.rollTypes.normal"),
            callback: html => {
              resolve(NemRoll.rollPool(html[0].querySelector('nem-pool').pool));
            },
          },

          'lBoon': {
            label: game.i18n.localize("nem.rollTypes.lBoon"),
            callback: html => {
              resolve(NemRoll.rollPool(html[0].querySelector('nem-pool').pool, 'lBoon'));
            },
          },

          'gBoon': {
            label: game.i18n.localize("nem.rollTypes.gBoon"),
            callback: html => {
              resolve(NemRoll.rollPool(html[0].querySelector('nem-pool').pool, 'gBoon'));
            },
          },
        },
        default: "roll",
        close: () => resolve({cancelled: true}),
      }, null).render(true);
    });
  }

  /*
   * Just make a simple roll a self compiled roll formula
   */
  static async simple({formula,
                       flavor=null,
                       speaker=null,
                       rollDrama=true}) {

    speaker = speaker || ChatMessage.getSpeaker();

    let r = new Roll(formula);
    let result = await r.roll({async: true}).then(() => {
      r.toMessage({
        flavor: flavor,
        speaker: speaker,
      });
    });

    if (rollDrama) { NemRoll.drama(speaker); }
    return result;
  }

  /*
   * This makes a drama dice roll
   */
  static async drama({speaker=null, showDice=false}) {
    let message = 'No Drama';
    let dramaDice = '1d20';
    let minmax = dramaDice.split('d').map(i => parseInt(i, 10));

    let r = new Roll(dramaDice);
    r.roll();

    if (r.result == minmax[0]) { message = "Bad Drama"; }
    if (r.result == minmax[1]) { message = "Good Drama"; }

    if (showDice) {
      r.toMessage({
        speaker: speaker || ChatMessage.getSpeaker(),
        flavor: message,
      })
    } else {
      ChatMessage.create({
        speaker: speaker || ChatMessage.getSpeaker(),
        content: message,
      });
    }
  }
}
