
export class NemItem extends Item {
  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareData() {
    super.prepareData();

    if (this.data.type === 'skill') { this._prepareSkillData(this.data.data); }
    else if (this.data.type === 'item') { this._prepareItemData(this.data.data); }
  }

  _prepareSkillData(itemData) {
  }

  _prepareItemData(itemData) {
  }
}


export class NemItemSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["nemron", "sheet", "item-sheet"],
      width: 550,
      height: 550,
    });
  }

  /** @override */
  get template() {
    const path = "systems/nemron/templates/item";
    return `${path}/${this.item.data.type}-sheet.hbs`;
  }

  /* -------------------------------------------- */

  /** Override the default renderer */

  /** @override */
  getData() {
    const baseData = super.getData();
    let sheetData = {};

    // Meta Stuff
    sheetData.config = CONFIG.nemron;

    // Item Stuff
    sheetData.item = baseData.data;
    sheetData.stats = baseData.data.data;

    return sheetData;
  }

  /* Form Submission */
  // _updateObject(ev, formData) {
  //   super._updateObject(ev, formData);
  // }

  /** @override */
  // setPosition(options = {}) {
  //   const position = super.setPosition(options);
  //   const sheetBody = this.element.find(".sheet-body");
  //   const bodyHeight = position.height - 192;
  //   sheetBody.css("height", bodyHeight);
  //   return position;
  // }

  /* -------------------------------------------- */

  /** @override */
  // activateListeners(html) {
  //   super.activateListeners(html);
  //   // Roll handlers, click handlers, etc. would go here.
  //   // Everything below here is only needed if the sheet is editable
  //   if (!this.options.editable) return;
  // }
}
