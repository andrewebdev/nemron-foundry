import { NemRoll } from './utils.js';


export class NemCombatant extends Combatant {
  _onCreate(data, options, userId) {
    super._onCreate(data, options, userId);
  }

  _getInitiativeFormula() {
    if ( this.actor ) return this.actor.getInitiativeFormula();
    return super._getInitiativeFormula();
  }

  async takeAction(pool) {
    // Spend initiative and Energy
    // const initiativeCost = this.getFlag("nem", "initiativeCost");
    const newInit = this.initiative - pool.initiative;

    // Update the token actor energy
    if (pool.energyCost > 0) {
      // TODO: How to update the actor energy?
      // We cannot just grab the _token._actor and update it's data directly
      // we have to use updateEmbeddedDocuments possibly?
      // For the time being players will need to do this manually.
    }

    return this.update({
      initiative: newInit,
      // ["flags.nem.initiativeCost"]: 0,  // The default
    });
  }

  async setState(data) {
    return this.update({
      initiative: data.initiative,
      // ["flags.nem.initiativeCost"]: data.initiativeCost,
    });
  }

  getState() {
    return {
      id: this.id,
      initiative: this.initiative,
      // initiativeCost: this.getFlag("nem", "initiativeCost"),
    }
  }

}

export class NemCombat extends Combat {

  _sortCombatants(a, b) {
    // We need to override this to ensure that players go first when
    // they have same initiative as npcs.
    const initA = Number.isNumeric(a.initiative) ? a.initiative : -9999;
    const initB = Number.isNumeric(b.initiative) ? b.initiative : -9999;

    let initDiff = initB - initA;
    if (initDiff != 0) { return initDiff; }

    const typeA = a.actor.data.type;
    const typeB = b.actor.data.type;

    // Player always go before NPC go first
    if (typeA != typeB) {
      if (typeA == "character") return -1;
      if (typeB == "character") return 1;
    }

    return a.tokenId - b.tokenId;
  }

  async rollInitiative(ids, formulaopt, updateTurnopt, msgOptionsopt) {
    await super.rollInitiative(ids, formulaopt, updateTurnopt, msgOptionsopt);
    return this.update({turn: 0});
  }

  async startCombat() {
    await this.setupTurns();
    // await this.setFlag("nem", "turnHistory", []);
    return super.startCombat();
  }

  async nextTurn() {
    let combatant = this.combatant;
    if (combatant.initiative <= 0) {
      return this.nextRound();
    }

    // await this.combatant.takeAction();
    return this.update({turn: 0});
  }
}

export class NemCombatTracker extends CombatTracker {

  get template() {
    return "systems/nemron/templates/nem-combat-tracker.hbs";
  }

  activateListeners(html) {
    super.activateListeners(html);
    html.find('.combatant-control.action').click(ev => {
      const el = ev.currentTarget;
      const c = this.viewed.combatants.get(el.dataset.combatantId);
      NemRoll.action({token: c._token}).then(result => {
        c.takeAction(result.pool);
      });
    });
  }

  //
  // async getData(options) {
  //   const data = await super.getData(options);

  //   if (!data.hasCombat) return data;

  //     data.turns[i].actionCost = combatant.getFlag("nem", "actionCost");

  //   return data;
  // }
}
