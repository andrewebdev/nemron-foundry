export const nem = {};

nem.generic = {
  edit: "nem.generic.edit",
  delete: "nem.generic.delete",
  energyLabel: "nem.generic.energyLabel",
  newItem: "nem.generic.newItem",
  frameLabel: "nem.generic.frameLabel",
  backgroundLabel: "nem.generic.backgroundLabel",
  attributeAffinityLabel: "nem.generic.attributeAffinityLabel",
  skillAffinityLabel: "nem.generic.skillAffinityLabel",
  roll: "nem.generic.roll",
  cancel: "nem.generic.cancel",
};

nem.rollTypes = {
  normal: "nem.rollTypes.normal",
  lBoon: "nem.rollTypes.lBoon",
  gBoon: "nem.rollTypes.gBoon",
  lPenalty: "nem.rollTypes.lPenalty",
  gPenalty: "nem.rollTypes.gPenalty",
};

nem.dialogs = {
  actionDialog: {
    title: "nem.dialogs.actionDialog.title",
  },
};

nem.attr = {
  body: {
    power: "nem.attr.body.power",
    agility: "nem.attr.body.agility",
    constitution: "nem.attr.body.constitution",
  },

  mind: {
    intellect: "nem.attr.mind.intellect",
    awareness: "nem.attr.mind.awareness",
    willpower: "nem.attr.mind.willpower",
  },

  spirit: {
    control: "nem.attr.spirit.control",
    sense: "nem.attr.spirit.sense",
    focus: "nem.attr.spirit.focus",
  }
};

nem.armorTypes = {
  light: 'nem.armorTypes.light',
  medium: 'nem.armorTypes.medium',
  heavy: 'nem.armorTypes.heavy',
};

nem.damageTypes = {
  corrosive: 'nem.damageTypes.corrosive',
  bludgeoning: 'nem.damageTypes.bludgeoning',
  cold: 'nem.damageTypes.cold',
  heat: 'nem.damageTypes.heat',
  kinetic: 'nem.damageTypes.kinetic',
  piercing: 'nem.damageTypes.piercing',
  slashing: 'nem.damageTypes.slashing',
  toxic: 'nem.damageTypes.toxic',
};

nem.nodeTypeLabel = "nem.nodeTypeLabel";
nem.nodeTypes = {
  attribute: "nem.nodeTypes.attribute",
  active: "nem.nodeTypes.active",
  passive: "nem.nodeTypes.passive",
  effect: "nem.nodeTypes.effect",
  fixed: "nem.nodeTypes.fixed",
  exclusive: "nem.nodeTypes.exclusive",
};

nem.skillTiers = {
  untrained: "nem.skillTiers.untrained",
  novice: "nem.skillTiers.novice",
  adept: "nem.skillTiers.adept",
  master: "nem.skillTiers.master",
  grandmaster: "nem.skillTiers.grandmaster",
};

nem.skill = {
  description: 'nem.skill.description',
  level: 'nem.skill.level',
  nodeType: 'nem.skill.nodeType',
  actions: 'nem.skill.actions',
  bonusActions: 'nem.skill.bonusActions',
};

nem.modifier = {
  description: "nem.modifier.description",
  notes: "nem.modifier.notes",
  mod: "nem.modifier.mod",
};

nem.item = {
  level: "nem.item.level",
  description: "nem.item.description",
  rrp: "nem.item.rrp",
  quantity: "nem.item.quantity",
  weight: "nem.item.weight",
  energy: {
    max: "nem.item.max",
    value: "nem.item.value"
  },
  equipped: "nem.item.equipped"
};
