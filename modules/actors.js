import { NemRoll, DicePool } from './utils.js';


export class NemActor extends Actor {

  prepareData() {
    super.prepareData();

    // Make separate methods for each Actor type (character, npc, etc.) to keep
    // things organized.
    if (this.data.type === 'character' || this.data.type === 'npc') {
      this._preparePassives(this.data.data);
    }
  }

  _preparePassives(stats) {
    // Hitpoints
    stats.body.hp.max = (stats.body.constitution.value + 5);
    stats.mind.hp.max = (stats.mind.willpower.value + 5);
    stats.spirit.hp.max = (stats.spirit.focus.value + 5);

    // Energy Pool
    stats.energy.max = ([
      stats.body.constitution.value,
      stats.mind.willpower.value,
      stats.spirit.focus.value,
    ].reduce((a, b) => a + b, 0)) * 2;

    // Movement Speed
    stats.movespeed = (stats.body.agility.value + 3);
  }

  getSkills() {
    return this.items.filter(item => item.type === 'skill');
  }

  getInitiativeFormula() {
    const actorData = this.data.data;
    var pool = new DicePool();

    // Add the base awareness stat.
    pool.addNode({
      id: 'agility',
      rank: actorData.body.agility.value,
    });
    pool.addNode({
      id: 'awareness',
      rank: actorData.mind.awareness.value,
    });
    pool.addNode({
      id: 'sense',
      rank: actorData.spirit.sense.value,
    });

    // Do we have the combat ready skill?
    this.getSkills().forEach(skill => {
      if (skill.name.toLowerCase() === ('combatready' || 'combat ready')) {
        pool.addNode({
          id: skill.id,
          rank: skill.data.level,
        });
      }
    });

    // Add lesser boon for affinity
    if (actorData.affinities.attribute.toLowerCase() === "sense") {
      pool.addLesserBoon();
    }

    let formula = pool.getRollFormula();
    console.log(formula);
    return formula;
  }
}


export class NemActorSheet extends ActorSheet {

  constructor(...args) {
    super(...args);
  }

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["nemron", "sheet", "actor"],
      width: 600,
      height: 600,
      tabs: [{
        navSelector: ".tabs",
        contentSelector: ".sheet-body",
        initial: "attributes",
      }]
    });
  }

  get template() {
    const path = "systems/nemron/templates/actor";
    return `${path}/${this.actor.data.type}-sheet.hbs`;
  }

  getData(opts) {
    const baseData = super.getData(opts);
    let sheetData = {};

    // Meta Stuff
    sheetData.config = CONFIG.nemron;

    // Character Stuff
    sheetData.actor = baseData.actor;
    sheetData.stats = baseData.data.data;
    sheetData.equipment = baseData.items.filter(item => item.type === 'item');
    sheetData.skills = baseData.items.filter(item => item.type === 'skill');

    return sheetData;
  }

  // Activate
  activateListeners(html) {
    super.activateListeners(html);

    html.find('[roll-action]').click(ev => {
      ev.preventDefault();
      let pool = html[0].querySelector('nem-pool').pool;
      let rollType = ev.currentTarget.getAttribute('roll-action');
      game.nemron.NemRoll.rollPool(pool, rollType);
    });

    // Only if the sheet is editable
    if (this.isEditable) {
      html.find('[inline-edit]').change(ev => {
        ev.preventDefault();

        let el = ev.currentTarget;
        let field = el.dataset.field;
        let obj = this.actor;

        if (el.dataset.itemId) {
          obj = this.actor.items.get(el.dataset.itemId);
        }

        if (el.getAttribute('type') === "checkbox") {
          return obj.update({[field]: el.checked});
        }

        return obj.update({[field]: el.value});
      });

      html.find('[create-item]').click(ev => {
        ev.preventDefault();
        let el = ev.currentTarget;
        let itemData = {
          name: game.i18n.localize("nem.generic.newItem"),
          type: el.dataset.itemType,
        };

        this.actor.createEmbeddedDocuments("Item", [itemData]).then(createdList => {
          createdList[0].sheet.render(true);
        });
      });

      new ContextMenu(html, "[item-context]", [
        {
          name: game.i18n.localize('nem.generic.edit'),
          icon: '<i class="fas fa-edit"></i>',
          callback: el => {
            const item = this.actor.items.get(el.data('itemId'));
            item.sheet.render(true);
          },
        },
        {
          name: game.i18n.localize('nem.generic.delete'),
          icon: '<i class="fas fa-trash-alt"></i>',
          callback: el => {
            this.actor.deleteOwnedItem(el.data("itemId"));
          },
        }
      ]);
    }
  }
}
