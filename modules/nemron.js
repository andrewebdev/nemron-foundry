/**
 * The Nemron game system for Foundry Virtual Tabletop
 * Author: Andre Engelbrecht <litt.fire.sa@gmail.com>
 * Software License: GNU GPLv3
 * Content License: (link our creative commons license)
 * Repository:
 * Issue Tracker:
 */
import { nem } from './config.js';
import { NemRoll } from './utils.js';
import { NemActorSheet, NemActor } from './actors.js';
import { NemItemSheet, NemItem } from './items.js';
import { NemCombat, NemCombatant, NemCombatTracker } from './combat.js';
import '../dist/nemron-elements.js';


Hooks.once("init", async () => {
  // Create our system namespace
  game.nemron = {
    NemActor,
    NemItem,
    NemRoll,
  };

  CONFIG.nemron = nem;

  CONFIG.Actor.documentClass = NemActor;
  CONFIG.Item.documentClass = NemItem;

  // Set up the Custom combat tracker
  CONFIG.Combat.documentClass = NemCombat;
  CONFIG.Combatant.documentClass = NemCombatant;
  CONFIG.ui.combat = NemCombatTracker;

  // // Register our system classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("nemron", NemActorSheet, {
    types: ["character", "npc"],
    makeDefault: true
  });

  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("nemron", NemItemSheet, {
    types: ["skill", "item"],
    makeDefault: true
  });
});
