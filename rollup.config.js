import resolve from 'rollup-plugin-node-resolve';
// import commonjs from 'rollup-plugin-commonjs';
// import babel from 'rollup-plugin-babel';
// import { terser } from 'rollup-plugin-terser';
// import { injectManifest } from 'rollup-plugin-workbox';

export default [
  {
    input: 'nemron-elements.js',
    output: {
      file: 'dist/nemron-elements.js',
      format: 'iife',
      sourcemap: false,
      compact: false,
      // plugins: [
      //   terser(),
      // ]
    },
    plugins: [
      resolve({
        customResolveOptions: {
          moduleDirectory: 'node_modules',
        }
      }),
      // commonjs(),
    ],
  },
];
