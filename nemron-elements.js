/*
 * Global custom elements used by our system
 */
import { LitElement, html, css } from 'lit';
import { repeat } from 'lit/directives/repeat.js';
import { DicePool } from './modules/utils.js';


class NemNode extends LitElement {

  static get properties() {
    return {
      nodeId: {
        type: String,
      },

      nodeName: {
        type: String
      },

      nodeType: {
        type: String,
        reflect: true
      },

      nodeLevel: {
        type: Number,
      },

      initiative: {
        type: Boolean,
      },

      energy: {
        type: Number,
      }
    };
  }

  constructor() {
    super();
    this.nodeId = '';
    this.nodeName = null;
    this.nodeType = 'active';
    this.nodeLevel = 0;
    this.initiative = true;
    this.energy = 0;

    this.addEventListener('click', (ev) => {
      this.classList.toggle('selected');
      const detail = {
        id: this.nodeId,
        name: this.nodeName,
        type: this.nodeType,
        level: this.nodeLevel,
        initiative: this.initiative,
        energy: this.energy,
      };

      if (this.classList.contains('selected')) {
        this.dispatchEvent(new CustomEvent('node-selected', {
          bubbles: true,
          composed: true,
          detail: detail,
        }));
      } else {
        this.dispatchEvent(new CustomEvent('node-deselected', {
          bubbles: true,
          composed: true,
          detail: detail,
        }));
      }
    });
  }

  static styles = css`
:host {
  box-sizing: border-box;
  display: block;
  padding: 5px;

  color: var(--nem-node-fg);
  background-color: var(--nem-node-bg);
}

:host(.selected) {
  color: var(--nem-node-selected-fg);
  background-color: var(--nem-node-selected-bg);
}

span { text-transform: capitalize; }
`;

  render() {
    // let nodeDice = html`${DicePool.dScale[this.nodeLevel]}`;
    var energy = html``;
    if (this.energy > 0) {
      energy = html`<span class="energy"><i class="fas fa-superpowers"></i> ${this.energy}</span>`
    }
    return html`<span>
${this.initiative ? html`<i class="fas fa-bullseye"></i>` : html``}
${this.nodeName}
(${this.nodeLevel})
${energy}
</span>`;
  }
}

if (typeof customElements.get('nem-node') === 'undefined') {
  customElements.define('nem-node', NemNode);
}

class NemPool extends LitElement {

  static get properties() {
    return {
      actorId: {
        type: String
      },

      tokenId: {
        type: String
      },

      initiativeCost: {
        type: Number
      },

      energyCost: {
        type: Number
      }
    };
  }

  constructor() {
    super();
    this.actorId = null;
    this.tokenId = null;
    this.actor = null;
    this.actorData = null;
    this.initiativeCost = 0;
    this.energyCost = 0;
    this._pool = new DicePool();
  }

  connectedCallback() {
    super.connectedCallback();

    if (this.actorId) {
      this.actor = game.actors.get(this.actorId);
    } else {
      // Given only the token ID, find the token within the game scene
      let token = game.scenes.active.data.tokens.find(t => t.id === this.tokenId);
      this.actor = token.getActor();
    }

    this.actorData = this.actor.data;

    if (this.actor) this._initNodes();
    this.updateSummary();
  }

  /*
   * This method will set up the nodes for the actor
   * Initially We will create 3 node groups: Attribute, Skill, Item
   */
  _initNodes() {
    this.body = this.actorData.data.body;
    this.mind = this.actorData.data.mind;
    this.spirit = this.actorData.data.spirit;

    this.itemNodes = this.actorData.items.filter(item => item.type === 'item');
    this.skillNodes = this.actorData.items.filter(item => item.type === 'skill');
  }

  static styles = css`
:host {
  position: relative;
  display: block;
  min-height: 300px;
}

header img {
  height: 50px;
  width: 50px;
  object-fit: cover;
  margin-right: 10px;
  vertical-align: middle;
}

.node-list {
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-between;
}

h3:first-child {
  width: 100%;
}

nem-node {
  width: 30%;
}
  `;

  render() {
    return html`
<div id="pool">
  <ul>
    <li>Initiative Cost: ${this.initiativeCost}</li>
    <li>Energy Cost: ${this.energyCost}</li>
  </ul>
</div>

<div id="attrNodes" class="node-list">
  <h3>Attributes</h3>
  ${this.renderAttrNodes()}
</div>

<div id="skillNodes" class="node-list">
  <h3>Skills</h3>
  ${repeat(this.skillNodes, n => n.id, (node, i) => html`
      <nem-node
        nodeType="${node.data.data.nodeType}"
        nodeLevel="${node.data.data.level}"
        nodeName="${node.name}"
        nodeId="${node.id}"
        initiative="${node.data.data.useInitiative}"
        energy="${node.data.data.energyCost}"
        @node-selected=${this._handleNodeSelected}
        @node-deselected=${this._handleNodeDeselected}>
      </nem-node>
    `)}
</div>

<div id="itemNodes" class="node-list">
  <h3>Items</h3>
  ${repeat(this.itemNodes, n => n.id, (node, i) => html`
      <nem-node
        nodeType="${node.data.data.nodeType}"
        nodeLevel="${node.data.data.level}"
        nodeName="${node.name}"
        nodeId="${node.id}"
        initiative="${node.data.data.useInitiative}"
        energy="${node.data.data.energyCost}"
        @node-selected=${this._handleNodeSelected}
        @node-deselected=${this._handleNodeDeselected}>
      </nem-node>
    `)}
</div>
    `;
  }

  _renderLine(line) {
    let nodes = [];
    for (let k in line) {
      if (k !== 'hp') {
        nodes.push(html`
          <nem-node
            nodeType="attribute"
            nodeLevel="${line[k].value}"
            nodeName="${k}"
            nodeId="${k}"
            initiative="true"
            energy="0"
            @node-selected=${this._handleNodeSelected}
            @node-deselected=${this._handleNodeDeselected}>
          </nem-node>
        `);
      }
    }
    return nodes;
  }

  renderAttrNodes() {
    return html`
      ${this._renderLine(this.body)}
      ${this._renderLine(this.mind)}
      ${this._renderLine(this.spirit)}
    `;
  }

  get pool() { return this._pool; }

  updateSummary() {
    this.initiativeCost = this._pool.initiative;
    this.energyCost = this._pool.energyCost;
  }

  _handleNodeSelected(ev) {
    this._pool.addNode({
      id: ev.detail.id,
      rank: ev.detail.level,
      useInitiative: ev.detail.initiative,
      energyCost: ev.detail.energy,
    });
    this.updateSummary();
  }

  _handleNodeDeselected(ev) {
    this._pool.removeNode(ev.detail.id);
    this.updateSummary();
  }
}

if (typeof customElements.get('nem-pool') === 'undefined') {
  customElements.define('nem-pool', NemPool);
}
